"use strict";
// ==UserScript==
// @name         Discord Soundboard X
// @version      0.1
// @description  Soundboard for discord voice
// @author       illemonati
// @match        https://discord.com/*
// @match        https://example.com/
// @match        https://online-voice-recorder.com/
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==
(function () {
    "use strict";
    const audioContext = new AudioContext();
    const gainNode = audioContext.createGain();
    const destNode = audioContext.createMediaStreamDestination();
    gainNode.gain.value = 0.2;
    gainNode.connect(destNode);
    // connect to self speakers
    gainNode.connect(audioContext.destination);
    let gainNodeSpeakerConnected = true;
    const sineNode = audioContext.createOscillator();
    sineNode.type = "sine";
    sineNode.frequency.value = 440;
    sineNode.start();
    let mediaNode = null;
    (function (oldGetUserMedia) {
        navigator.mediaDevices.getUserMedia = async function () {
            audioContext.resume();
            const stream = await oldGetUserMedia.apply(this, arguments);
            console.log(stream);
            const micSrcNode = audioContext.createMediaStreamSource(stream);
            micSrcNode.connect(destNode);
            console.log(destNode.stream);
            return destNode.stream.clone();
        };
    })(navigator.mediaDevices.getUserMedia);
    const playSine = () => {
        sineNode.connect(gainNode);
    };
    const stopSine = () => {
        sineNode.disconnect(gainNode);
    };
    const soundboardHTML = `
        <style>
            .soundboard {
                position: absolute;
                top: 50vh;
                left: 50vw;
                transform: translate(-50%, -50%);
                width: 50vw;
                height: 50vh;
                background: rgba(255, 255, 255, 0.5);
                z-index: 9999999999999;
            }
        </style>
        <div class="soundboard">
            <button class="soundboard-play-sine">Play SINE</button>
            <button class="soundboard-stop-sine">Stop SINE</button>
            <br />
            <input type="file" class="soundboard-file-input"/>
            <br />
            <button class="soundboard-play-file">Play FILE</button>
            <button class="soundboard-stop-file">Stop FILE</button>
            <br />
            <input type="range" class="soundboard-volume" name="volume" min="0" max="100"/>
            <br />
            <input type="checkbox" class="soundboard-allow-self-hear" name="soundboard-allow-self-hear"/>
            <label for="soundboard-allow-self-hear">Allow Self Hear</label>
        </div>
    `;
    const toggleSoundBoard = () => {
        const existing = document.querySelector(".soundboard-wrapper");
        if (existing) {
            existing.remove();
            return;
        }
        const soundBoardDivWrapper = document.createElement("div");
        soundBoardDivWrapper.classList.add("soundboard-wrapper");
        soundBoardDivWrapper.innerHTML = soundboardHTML;
        soundBoardDivWrapper
            .querySelector(".soundboard-play-sine")
            ?.addEventListener("click", () => {
            playSine();
        });
        soundBoardDivWrapper
            .querySelector(".soundboard-stop-sine")
            ?.addEventListener("click", () => {
            stopSine();
        });
        const playFile = () => {
            const input = soundBoardDivWrapper.querySelector(".soundboard-file-input");
            if (!input || !input.files || input.files.length < 1) {
                alert("error");
                return;
            }
            const url = URL.createObjectURL(input.files[0]);
            const audioEle = document.createElement("audio");
            audioEle.src = url;
            mediaNode = audioContext.createMediaElementSource(audioEle);
            mediaNode.connect(gainNode);
            audioEle.play();
        };
        const stopFile = () => {
            if (!mediaNode) {
                return;
            }
            mediaNode.disconnect(gainNode);
        };
        soundBoardDivWrapper
            .querySelector(".soundboard-play-file")
            ?.addEventListener("click", () => {
            playFile();
        });
        soundBoardDivWrapper
            .querySelector(".soundboard-stop-file")
            ?.addEventListener("click", () => {
            stopFile();
        });
        const volumeInput = soundBoardDivWrapper.querySelector(".soundboard-volume");
        if (volumeInput) {
            volumeInput.addEventListener("input", (e) => {
                gainNode.gain.value =
                    parseInt(e.target.value) / 100;
            });
            volumeInput.value = (gainNode.gain.value * 100).toString();
            document.body.appendChild(soundBoardDivWrapper);
        }
        const selfHearInput = soundBoardDivWrapper.querySelector(".soundboard-allow-self-hear");
        if (selfHearInput) {
            selfHearInput.addEventListener("change", (e) => {
                if (e.target.checked) {
                    gainNode.connect(audioContext.destination);
                    gainNodeSpeakerConnected = true;
                }
                else {
                    gainNode.disconnect(audioContext.destination);
                    gainNodeSpeakerConnected = false;
                }
            });
            selfHearInput.checked = gainNodeSpeakerConnected;
        }
    };
    document.addEventListener("keydown", (e) => {
        if (e.ctrlKey && e.shiftKey && e.key == "S") {
            toggleSoundBoard();
        }
    });
})();
